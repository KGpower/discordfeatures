const Discord = require('discord.js');
var auth = require('./auth.json');
var arguments = process.argv.slice(2)
var authToken = arguments[0]
var delimiter = arguments[1]

const MESSAGE = 0
const HOUR = 1
const MINUTE = 2
const SECOND = 3
const END = 4
const START = 1

class TimerBot extends Discord.Client {
    constructor() {
        super({token: authToken, autorun: true});
        this.activeTimers = [];
        this.activeStopwatches = [];
        this.activeAlarms = [];
        this.checkStarted = false;
    }
}

var client = new TimerBot()

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms)
    })
}

async function checkTimers(){
    client.checkStarted = true
    while(true){
        if(client.activeTimers.length > 0){
            var currentTime = Date.now()
            client.activeTimers.forEach((timer, index) => {
                if(timer[END] < currentTime){
                    if(timer[HOUR] == 0 && timer[MINUTE] == 0 && timer[SECOND] > 1){
                        timer[MESSAGE].reply("Your "+timer[SECOND]+" Second Timer Has Finished");
                    }
                    if(timer[HOUR] == 0 && timer[MINUTE] > 0 && timer[SECOND] == 0){
                        timer[MESSAGE].reply("Your "+timer[MINUTE]+" Minute Timer Has Finished");
                    }
                    if(timer[HOUR] > 0 && timer[MINUTE] == 0 && timer[SECOND] == 0){
                        timer[MESSAGE].reply("Your "+timer[HOUR]+" Hour Timer Has Finished");
                    }
                    if(timer[HOUR] == 0 && timer[MINUTE] > 0 && timer[SECOND] > 1){
                        timer[MESSAGE].reply("Your "+timer[MINUTE]+" Minute and "+timer[SECOND]+" Second Timer Has Finished");
                    }
                    if(timer[HOUR] > 0 && timer[MINUTE] > 0 && timer[SECOND] == 0){
                        timer[MESSAGE].reply("Your "+timer[HOUR]+" Hour and "+timer[MINUTE]+" Minute Timer Has Finished");
                    }
                    if(timer[HOUR] > 0 && timer[MINUTE] == 0 && timer[SECOND] > 0){
                        timer[MESSAGE].reply("Your "+timer[HOUR]+" Hour and "+timer[SECOND]+" Second Timer Has Finished");
                    }
                    if(timer[HOUR] > 0 && timer[MINUTE] > 0 && timer[SECOND] > 0){
                        timer[MESSAGE].reply("Your "+timer[HOUR]+" Hour, "+timer[MINTUE]+" Minute and "+timer[SECOND]+" Second Timer Has Finished");
                    }
                    client.activeTimers.splice(index, 1);
                }
            })
        }
        else {
            break
        }
        await sleep(500);
    }
    client.checkStarted = false
}

client.on('disconnect', function(erMsg, code) {
    console.log('----- TimerBot disconnected from Discord with code', code, 'for reason:', erMsg, '-----');
    TimerBot.connect();
});

client.on('ready', () => {
    console.log('----- TimerBot Connected. Logged In As: '+client.user.tag+" -----");
});

client.on('message', message => {
    if(message.author == client.user){
        return;
    }
    else{
        var content = message.content.toUpperCase()
        if(content.startsWith(delimiter+"STOPWATCH")){
            console.log("Stopwatch Request From User "+message.author);
            var commands = content.split(" ");
            if(commands.length == 2){
                if(commands[1] == "START"){
                    client.activeStopwatches.forEach((stopwatch) => {
                        if(stopwatch[MESSAGE].author == message.author){
                            var time = Date.now();
                            var time_elapsed = (time - stopwatch[START]) / 1000;
                            message.reply("You already have a stopwatch in this text channel. Time elapsed: **"+time_elapsed+"** Seconds");
                            return;
                        }
                    });
                    var start = Date.now();
                    var stopwatch = [message, start];
                    client.activeStopwatches.push(stopwatch);
                    message.reply("Stopwatch Started");
                }
                else if(commands[1] == "STOP"){
                    if(client.activeStopwatches.length > 0){
                        client.activeStopwatches.forEach((stopwatch, index) => {
                            if(stopwatch[MESSAGE].author == message.author){
                                var time = Date.now();
                                var time_elapsed = (time - stopwatch[START]) / 1000;
                                message.reply("Stopwatch Stopped. Time Elapsed: **"+time_elapsed+"** Seconds");
                                client.activeStopwatches.splice(index, 1)
                            }
                        })
                    }
                    else{
                        message.reply("There are no active stopwatches in this text channel");
                    }
                }
            }
        }
        if (content.startsWith(delimiter+"TIMER")){
            console.log("Timer Request From User "+message.author);
            var commands = content.split(" ");
            if(commands.length == 2){
                var t = NaN
                if(t = Number(commands[1])){
                    console.log(t)
                    if(t == 1){
                        message.reply("Starting a Timer for: **"+t+" Second**");
                    }
                    else{
                        message.reply("Starting a Timer for: **"+t+" Seconds**");
                    }
                    var end = Date.now() + (t * 1000);
                    var timer = [message, 0, 0, t, end];
                    client.activeTimers.push(timer);
                }
                else if(commands[1] == "TOMORROW"){
                    t = 24 * 60 * 60;
                    message.reply("Starting a Timer for: **24 Hours**");
                    var end = Date.now() + (t * 1000);
                    var timer = [message, 24, 0, 0, end]
                    client.activeTimers.push(timer)
                }
            }
            else if(commands.length == 3){
                if(commands[2] == "H"){
                    var t = NaN
                    if(t = Number(commands[1])){
                        var s = t * 60 * 60;
                        if(t == 1){
                            message.reply("Starting a Timer for: **"+t+" Hour**");
                        }
                        else{
                            message.reply("Starting a Timer for: **"+t+" Hours**");
                        }
                        var end = Date.now() + (s * 1000);
                        var timer = [message, t, 0, 0, end];
                        client.activeTimers.push(timer);
                    }
                }
                else if(commands[2] == "M"){
                    var t = NaN
                    if(t = Number(commands[1])){
                        var s = t * 60;
                        if(t == 1){
                            message.reply("Starting a Timer for: **"+t+" Minute**");
                        }
                        else{
                            message.reply("Starting a Timer for: **"+t+" Minutes**");
                        }
                        var end = Date.now() + (s * 1000);
                        var timer = [message, 0, t, 0, end];
                        client.activeTimers.push(timer);
                    }
                }
                else if(commands[2] == "S"){
                    var t = NaN
                    if(t = Number(commands[1])){
                        if(t == 1){
                            message.reply("Starting a Timer for: **"+t+" Second**");
                        }
                        else{
                            message.reply("Starting a Timer for: **"+t+" Seconds**");
                        }
                        var end = Date.now() + (t * 1000);
                        var timer = [message, 0, 0, t, end];
                        client.activeTimers.push(timer);
                    }
                }
            }
            else if(commands.length == 5){
                if(commands[2] == "H" && commands[4] == "M"){
                    var h = NaN
                    var m = NaN
                    if((h = Number(commands[1])) && (m = Number(commands[3]))){
                        var t = (h * 60 * 60) + (m * 60);
                        if(h == 1 && m == 1){
                            message.reply("Starting a Timer for: **1 Hour and 1 Minute**");
                        }
                        else if(h == 1 && m > 1){
                            message.reply("Starting a Timer for: **1 Hour and "+m+" Minutes**");
                        }
                        else if(h > 1 && m == 1){
                            message.reply("Starting a Timer for: **"+h+" Hours and 1 Minute**");
                        }
                        else if(h > 1 && m > 1){
                            message.reply("Starting a Timer for: **"+h+" Hours and "+m+" Minutes**");
                        }
                        var end = Date.now() + (t * 1000);
                        var timer = [message, h, m, 0, end];
                        client.activeTimers.push(timer);
                    }
                }
                else if(commands[2] == "H" && commands[4] == "S"){
                    var h = NaN
                    var s = NaN
                    if((h = Number(commands[1])) && (s = Number(commands[3]))){
                        var t = (h * 60 * 60) + (s);
                        if(h == 1 && s == 1){
                            message.reply("Starting a Timer for: **1 Hour and 1 Second**");
                        }
                        else if(h == 1 && s > 1){
                            message.reply("Starting a Timer for: **1 Hour and "+s+" Seconds**");
                        }
                        else if(h > 1 && s == 1){
                            message.reply("Starting a Timer for: **"+h+" Hours and 1 Second**");
                        }
                        else if(h > 1 && s > 1){
                            message.reply("Starting a Timer for: **"+h+" Hours and "+s+" Seconds**");
                        }
                        var end = Date.now() + (t * 1000);
                        var timer = [message, h, 0, s, end];
                        client.activeTimers.push(timer);
                    }
                }
                else if(commands[2] == "M" && commands[4] == "S"){
                    var m = NaN
                    var s = NaN
                    if((m = Number(commands[1])) && (s = Number(commands[3]))){
                        var t = (h * 60) + (s);
                        if(m == 1 && s == 1){
                            message.reply("Starting a Timer for: **1 Minute and 1 Second**");
                        }
                        else if(m == 1 && s > 1){
                            message.reply("Starting a Timer for: **1 Minute and "+s+" Seconds**");
                        }
                        else if(m > 1 && s == 1){
                            message.reply("Starting a Timer for: **"+m+" Minutes and 1 Second**");
                        }
                        else if(m > 1 && s > 1){
                            message.reply("Starting a Timer for: **"+m+" Minutes and "+s+" Seconds**");
                        }
                        var end = Date.now() + (t * 1000);
                        var timer = [message, 0, m, s, end];
                        client.activeTimers.push(timer);
                    }
                }
            }
            else if(commands.length == 7){
                var h = NaN
                var m = NaN
                var s = NaN
                if((h = Number(commands[1])) && (m = Number(commands[3])) && (s = Number(commands[5]))){
                    t = (h * 60 * 60) + (m * 60) + s;
                    if(h == 1 && m == 1 && s == 1){
                        message.reply("Starting a Timer for: **"+h+" Hour, "+m+" Minute and "+s+" Second**");
                    }
                    else if(h == 1 && m == 1 && s > 1){
                        message.reply("Starting a Timer for: **"+h+" Hour, "+m+" Minute and "+s+" Seconds**");
                    }
                    else if(h == 1 && m > 1 && s == 1){
                        message.reply("Starting a Timer for: **"+h+" Hour, "+m+" Minutes and "+s+" Second**");
                    }
                    else if(h > 1 && m == 1 && s == 1){
                        message.reply("Starting a Timer for: **"+h+" Hours, "+m+" Minute and "+s+" Second**");
                    }
                    else if(h == 1 && m > 1 && s > 1){
                        message.reply("Starting a Timer for: **"+h+" Hour, "+m+" Minutes and "+s+" Seconds**");
                    }
                    else if(h > 1 && m > 1 && s == 1){
                        message.reply("Starting a Timer for: **"+h+" Hours, "+m+" Minutes and "+s+" Second**");
                    }
                    else if(h > 1 && m == 1 && s > 1){
                        message.reply("Starting a Timer for: **"+h+" Hours, "+m+" Minute and "+s+" Seconds**");
                    }
                    else if(h > 1 && m > 1 && s > 1){
                        message.reply("Starting a Timer for: **"+h+" Hours, "+m+" Minutes and "+s+" Seconds**");
                    }
                    var end = Date.now() + (t * 1000);
                    var timer = [message, h, m, s, end];
                    client.activeTimers.push(timer);
                }
            }
            if(!client.checkStarted){
                checkTimers();
            }
        }
    }
});

client.login(auth.token)