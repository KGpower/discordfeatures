import discord
import time
import asyncio

TOKEN = 'NzExMjIxMzA5NjM1Mjk3Mjgw.Xr_4qg.Z_-wK6yjrgmsrh9ThMNOZy5Lgsg'

class BotClient(discord.Client):
    def __init__(self):
        super().__init__()
        self.active_timers = []
        self.active_stopwatches = []
        self.active_alarms = []
        self.check_started = False

    async def on_ready(self):
        print("Logged in as")
        print(self.user.name)
        print(self.user.id)
        print("--------------")

    async def check_loop(self):
        self.check_started = True
        while True:
            await self.check()
            await asyncio.sleep(0.5)
        self.check_started = False

    async def check(self):
        if len(self.active_timers) > 0:
            for timer in self.active_timers:
                if timer["End"] < time.time():
                    if timer["Hours"] > 0 and timer["Minutes"] > 0 and timer["Seconds"] > 0:
                        await timer["Channel"].send("{u.mention} your {h} hour, {m} minute, and {s} second timer has finished!".format(u=timer["User"], h=timer["Hours"], m=timer["Minutes"], s=timer["Seconds"]))
                    if timer["Hours"] > 0 and timer["Minutes"] > 0 and timer["Seconds"] == 0:
                        await timer["Channel"].send("{u.mention} your {h} hour and {m} minute has finished!".format(u=timer["User"], h=timer["Hours"], m=timer["Minutes"]))
                    if timer["Hours"] > 0 and timer["Minutes"] == 0 and timer["Seconds"] > 0:
                        await timer["Channel"].send("{u.mention} your {h} hour and {s} second timer has finished!".format(u=timer["User"], h=timer["Hours"], s=timer["Seconds"]))
                    if timer["Hours"] > 0 and timer["Minutes"] == 0 and timer["Seconds"] == 0:
                        await timer["Channel"].send("{u.mention} your {h} hour timer has finished!".format(u=timer["User"], h=timer["Hours"]))
                    if timer["Hours"] == 0 and timer["Minutes"] > 0 and timer["Seconds"] > 0:
                        await timer["Channel"].send("{u.mention} your {m} minute, and {s} second timer has finished!".format(u=timer["User"], m=timer["Minutes"], s=timer["Seconds"]))
                    if timer["Hours"] == 0 and timer["Minutes"] > 0 and timer["Seconds"] == 0:
                        await timer["Channel"].send("{u.mention} your {m} minute timer has finished!".format(u=timer["User"], m=timer["Minutes"]))
                    if timer["Hours"] == 0 and timer["Minutes"] == 0 and timer["Seconds"] > 0:
                        await timer["Channel"].send("{u.mention} your {s} second timer has finished!".format(u=timer["User"], s=timer["Seconds"]))
                    self.active_timers.remove(timer)

client = BotClient()

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if message.content.startswith("!calc"):
        content = message.content.split()
        if content[2] == "*" or content[2].upper() == "X":
            answer = round(float(content[1]) * float(content[3]), 2)
            await message.channel.send("{a} * {b} = {c}".format(a=content[1], b=content[3], c=answer))
        elif content[2] == "+":
            answer = round(float(content[1]) + float(content[3]), 2)
            await message.channel.send("{a} * {b} = {c}".format(a=content[1], b=content[3], c=answer))
        elif content[2] == "/":
            answer = round(float(content[1]) / float(content[3]), 2)
            await message.channel.send("{a} * {b} = {c}".format(a=content[1], b=content[3], c=answer))
        elif content[2] == "-":
            answer = round(float(content[1]) - float(content[3]), 2)
            await message.channel.send("{a} * {b} = {c}".format(a=content[1], b=content[3], c=answer))
    if message.content.startswith("!time"):
        await message.channel.send("{u.mention} The Current Time is: {t}".format(u=message.author, t=time.strftime("%H:%M:%S", time.localtime())))
    if message.content.startswith("!datetime"):
        await message.channel.send("{u.mention} The Current Time and Date is: {t}".format(u=message.author, t=time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime())))
    if message.content.startswith('!stopwatch'):
        print("Stopwatch Request: {c} - {u}".format(c=message.channel, u=message.author))
        content = message.content.split()
        if content[1].upper() == "START":
            if len(client.active_stopwatches) > 0:
                for watch in client.active_stopwatches:
                    if watch["User"] == message.author:
                        elapsed = round(time.time() - watch["Start"], 2)
                        await watch["Channel"].send("{u.mention} You Already Have A Timer In This Text Channel. It Is Currently At: {s} Seconds".format(u=watch["User"], s=elapsed))
                        break
                else:
                    start_time = time.time()
                    client.active_stopwatches.append({"User": message.author, "Channel": message.channel, "Start": start_time, "End": 0})
                    await message.channel.send("{u.mention} Timer Started.".format(u=message.author))
            else:
                start_time = time.time()
                client.active_stopwatches.append({"User": message.author, "Channel": message.channel, "Start": start_time, "End": 0})
                await message.channel.send("{u.mention} Timer Started.".format(u=message.author))
        elif content[1].upper() == "STOP":
            try:
                if len(client.active_stopwatches) > 0:
                    for watch in client.active_stopwatches:
                        if watch["User"] == message.author:
                            end = time.time()
                            elapsed = round(end - watch["Start"], 2)
                            await watch["Channel"].send("{u.mention} Timer Stopped. Time Elapsed: {s} Seconds".format(u=watch["User"], s=elapsed))
                            client.active_stopwatches.remove(watch)
                            break
                    else:
                        raise ValueError
                else:
                    raise ValueError
            except ValueError:
                await message.channel.send("No Active Stopwatches In This Text Channel")
        elif content[1].upper() == "LAP":
            pass
    if message.content.startswith('!alarm'):
        content = message.content.split()
    if message.content.startswith('!timer'):
        print("Timer Request: {c} - {u}".format(c=message.channel, u=message.author))
        content = message.content.split()
        if content[1].upper() == "TIME":
            if len(client.active_timers) > 0:
                for timer in client.active_timers:
                    if timer["User"] == message.author:
                        remaining = round(timer["End"] - time.time(), 2)

                        await timer["Channel"].send("{u.mention} your timer has {s} seconds remaining".format(u=timer["User"], s=remaining))
        elif content[1].upper() == "CLEAR_LAST":
            if len(client.active_timers) > 0:
                for timer in reversed(client.active_timers):
                    if timer["User"] == message.author:
                        client.active_timers.remove(timer)
                        break
        elif content[1].upper() == "CLEAR_FIRST":
            if len(client.active_timers) > 0:
                for timer in client.active_timers:
                    if timer["User"] == message.author:
                        client.active_timers.remove(timer)
                        break
        elif content[1].upper() == "CLEAR_ALL":
            if len(client.active_timers) > 0:
                for timer in reversed(client.active_timers):
                    if timer["User"] == message.author:
                        client.active_timers.remove(timer)
        elif content[1].upper() == "SET":
            pass
        else:
            if len(content) == 2:
                t = int(content[1])
                if t == 1:
                    await message.channel.send("Starting a Timer for: {t} Seconds".format(t=t))
                else:
                    await message.channel.send("Starting a Timer for: {t} Seconds".format(t=t))
                end = time.time() + t
                client.active_timers.append({"Channel": message.channel, "User": message.author, "Hours": 0, "Minutes": 0, "Seconds": t, "End": end})
            elif len(content) == 3:
                if content[2].upper() == "H":
                    t = int(content[1])
                    s = t * 60 * 60
                    if t == 1:
                        await message.channel.send("Starting a Timer for: {t} Hour".format(t=t))
                    else:
                        await message.channel.send("Starting a Timer for: {t} Hours".format(t=t))
                    end = time.time() + s
                    client.active_timers.append({"Channel": message.channel, "User": message.author, "Hours": t, "Minutes": 0, "Seconds": 0, "End": end})
                elif content[2].upper() == "M":
                    t = int(content[1])
                    s = t * 60
                    if t == 1:
                        await message.channel.send("Starting a Timer for: {t} Minute".format(t=t))
                    else:
                        await message.channel.send("Starting a Timer for: {t} Minutes".format(t=t))
                    end = time.time() + s
                    client.active_timers.append({"Channel": message.channel, "User": message.author, "Hours": 0, "Minutes": t, "Seconds": 0, "End": end})
                elif content[2].upper() == "S":
                    t = int(content[1])
                    if t == 1:
                        await message.channel.send("Starting a Timer for: {t} Second".format(t=t))
                    else:
                        await message.channel.send("Starting a Timer for: {t} Seconds".format(t=t))
                    end = time.time() + t
                    client.active_timers.append({"Channel": message.channel, "User": message.author, "Hours": 0, "Minutes": 0, "Seconds": t, "End": end})
            elif len(content) == 5:
                if content[2].upper() == "H" and content[4].upper() == "M":
                    h = int(content[1])
                    m = int(content[3])
                    t = (h * 60 * 60) + (m * 60)
                    if h == 1 and m == 1:
                        await message.channel.send("Starting a Timer for: {h} Hour and {m} Minute".format(h=h, m=m))
                    elif h == 1 and m > 1:
                        await message.channel.send("Starting a Timer for: {h} Hour and {m} Minutes".format(h=h, m=m))
                    elif h > 1 and m == 1:
                        await message.channel.send("Starting a Timer for: {h} Hours and {m} Minute".format(h=h, m=m))
                    else:
                        await message.channel.send("Starting a Timer for: {h} Hours and {m} Minutes".format(h=h, m=m))
                    end = time.time() + t
                    client.active_timers.append({"Channel": message.channel, "User": message.author, "Hours": h, "Minutes": m, "Seconds": 0, "End": end})
                elif content[2].upper() == "H" and content[4].upper() == "S":
                    h = int(content[1])
                    s = int(content[3])
                    t = (h * 60 * 60) + (s)
                    if h == 1 and s == 1:
                        await message.channel.send("Starting a Timer for: {h} Hour and {s} Second".format(h=h, s=s))
                    elif h == 1 and s > 1:
                        await message.channel.send("Starting a Timer for: {h} Hour and {s} Seconds".format(h=h, s=s))
                    elif h > 1 and s == 1:
                        await message.channel.send("Starting a Timer for: {h} Hours and {s} Second".format(h=h, s=s))
                    else:
                        await message.channel.send("Starting a Timer for: {h} Hours and {s} Seconds".format(h=h, s=s))
                    end = time.time() + t
                    client.active_timers.append({"Channel": message.channel, "User": message.author, "Hours": h, "Minutes": 0, "Seconds": s, "End": end})
                elif content[2].upper() == "M" and content[4].upper() == "S":
                    m = int(content[1])
                    s = int(content[3])
                    t = (m * 60) + (s)
                    if m == 1 and s == 1:
                        await message.channel.send("Starting a Timer for: {m} Minute and {s} Second".format(m=m, s=s))
                    elif m == 1 and s > 1:
                        await message.channel.send("Starting a Timer for: {m} Minute and {s} Seconds".format(m=m, s=s))
                    elif m > 1 and s == 1:
                        await message.channel.send("Starting a Timer for: {m} Minutes and {s} Second".format(m=m, s=s))
                    else:
                        await message.channel.send("Starting a Timer for: {m} Minutes and {s} Seconds".format(m=m, s=s))
                    end = time.time() + t
                    client.active_timers.append({"Channel": message.channel, "User": message.author, "Hours": 0, "Minutes": m, "Seconds": s, "End": end})
            elif len(content) == 7:
                    h = int(content[1])
                    m = int(content[3])
                    s = int(content[5])
                    t = (h * 60 * 60) + (m * 60) + (s)
                    if h == 1 and m == 1 and s == 1:
                        await message.channel.send("Starting a Timer for: {h} Hour, {m} Minute, and {s} Second".format(h=h, m=m, s=s))
                    elif h == 1 and m > 1 and s == 1:
                        await message.channel.send("Starting a Timer for: {h} Hour, {m} Minutes, and {s} Second".format(h=h, m=m, s=s))
                    elif h == 1 and m == 1 and s > 1:
                        await message.channel.send("Starting a Timer for: {h} Hour, {m} Minute, and {s} Seconds".format(h=h, m=m, s=s))
                    elif h == 1 and m > 1 and s > 1:
                        await message.channel.send("Starting a Timer for: {h} Hour, {m} Minutes, and {s} Seconds".format(h=h, m=m, s=s))
                    elif h > 1 and m == 1 and s > 1:
                        await message.channel.send("Starting a Timer for: {h} Hours, {m} Minute, and {s} Seconds".format(h=h, m=m, s=s))
                    elif h > 1 and m > 1 and s == 1:
                        await message.channel.send("Starting a Timer for: {h} Hours, {m} Minutes, and {s} Second".format(h=h, m=m, s=s))
                    elif h > 1 and m == 1 and s == 1:
                        await message.channel.send("Starting a Timer for: {h} Hours, {m} Minute, and {s} Second".format(h=h, m=m, s=s))
                    else:
                        await message.channel.send("Starting a Timer for: {h} Hours, {m} Minutes, and {s} Seconds".format(h=h, m=m, s=s))
                    end = time.time() + t
                    client.active_timers.append({"Channel": message.channel, "User": message.author, "Hours": h, "Minutes": m, "Seconds": s, "End": end})
            if not client.check_started:
                client.timer_check = client.loop.create_task(client.check_loop())
        

client.run(TOKEN)