const Discord = require('discord.js');
const apiToken = "975a8fef-fe46-4b02-bb8e-d684f04c3e18"
const request = require('request')
var arguments = process.argv.slice(2)
var authToken = arguments[0]
var delimiter = arguments[1]

class StatBot extends Discord.Client {
    constructor() {
        super({token: authToken, autorun: true});
        this.languages = [];
    }
}

var client = new StatBot()

client.on('disconnect', function(erMsg, code) {
    console.log('----- StatBot disconnected from Discord with code', code, 'for reason:', erMsg, '-----');
    TranslateBot.connect();
});

client.on('ready', () => {
    console.log('----- StatBot Connected. Logged In As: '+client.user.tag+" -----");
});

var messageEmbed = new Discord.MessageEmbed();

function call_api(url){
    request.get(url, {headers: { "TRN-Api-Key": apiToken, "Accept": "application/json"}},(error, response, body) => {
        if(error && response.statusCode != 200){
            console.error(response.statusCode);
            console.error(body);
        }
        else{
            var data = JSON.parse(body);
            console.log(body)
            return data   
        }
    });
}

client.on('message', message => {
    if(message.author == client.user){
        return;
    }
    else{
        var commands = message.content.split(" ");
        var url = ""
        if(commands[0] == delimiter+"gamestats"){
            if(commands.length > 2){
                if(commands[1].toUpperCase() == "APEX" || commands[1].toUpperCase() == "APEXLEGENDS"){
                    if(commands[2].toUpperCase() == "PC" || commands[1].toUpperCase() == "ORIGIN"){
                        var url = "https://public-api.tracker.gg/v2/apex/standard/profile/origin/" + commands[3]
                        data = call_api(url)
                        messageEmbed.setTitle(commands[3].toUpperCase())
                        messageEmbed.setDescription("Your APEX LEGENDS stats here:")
                        messageEmbed.setColor("#FF0000")
                        message.channel.send(messageEmbed)
                    }
                    else if(commands[2].toUpperCase() == "XBOX"){
                        var url = "https://public-api.tracker.gg/v2/apex/standard/profile/xbl/" + commands[3]
                        data = call_api(url)
                        messageEmbed.setTitle(commands[3].toUpperCase())
                        messageEmbed.setDescription("Your APEX LEGENDS stats here:")
                        messageEmbed.setColor("#FF0000")
                        message.channel.send(messageEmbed)
                    }
                    else if(commands[2].toUpperCase() == "PS4" || commands[1].toUpperCase() == "PLAYSTATION"){
                        var url = "https://public-api.tracker.gg/v2/apex/standard/profile/psn/" + commands[3]
                        data = call_api(url)
                        messageEmbed.setTitle(commands[3].toUpperCase())
                        messageEmbed.setDescription("Your APEX LEGENDS stats here:")
                        messageEmbed.setColor("#FF0000")
                        message.channel.send(messageEmbed)
                    }
                }
                if(commands[1].toUpperCase() == "CSGO"){
                    var url = "https://public-api.tracker.gg/v2/csgo/standard/profile/steam/" + commands[2]
                    data = call_api(url)
                    messageEmbed.setTitle(commands[2].toUpperCase())
                    messageEmbed.setDescription("Your CS:GO stats here:")
                    messageEmbed.setColor("#FF0000")
                    message.channel.send(messageEmbed)
                }
                if(commands[1].toUpperCase() == "OVERWATCH"){
                    if(commands[2].toUpperCase() == "PC" || commands[2].toUpperCase() == "BATTLENET"){
                        var url = "https://public-api.tracker.gg/v2/csgo/standard/profile/battlenet/" + commands[3]
                        data = call_api(url)
                        messageEmbed.setTitle(commands[3].toUpperCase())
                        messageEmbed.setDescription("Your OVERWATCH stats here:")
                        messageEmbed.setColor("#FF0000")
                        message.channel.send(messageEmbed)
                        console.log(data)
                    }
                    if(commands[2].toUpperCase() == "PS4" || commands[2].toUpperCase() == "PLAYSTATION"){
                        var url = "https://public-api.tracker.gg/v2/csgo/standard/profile/psn/" + commands[3]
                        data = call_api(url)
                        messageEmbed.setTitle(commands[3].toUpperCase())
                        messageEmbed.setDescription("Your OVERWATCH stats here:")
                        messageEmbed.setColor("#FF0000")
                        message.channel.send(messageEmbed)
                    }
                    if(commands[2].toUpperCase() == "XBOX"){
                        var url = "https://public-api.tracker.gg/v2/csgo/standard/profile/xbl/" + commands[3]
                        data = call_api(url)
                        messageEmbed.setTitle(commands[3].toUpperCase())
                        messageEmbed.setDescription("Your OVERWATCH stats here:")
                        messageEmbed.setColor("#FF0000")
                        message.channel.send(messageEmbed)
                    }
                }
            }
        }
    }
});

client.login(authToken)