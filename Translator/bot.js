const Discord = require('discord.js');
const translateURL = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl="
const request = require('request')
var arguments = process.argv.slice(2)
var authToken = arguments[0]
var delimiter = arguments[1]

class TranslateBot extends Discord.Client {
    constructor() {
        super({token: authToken, autorun: true});
    }
}

var client = new TranslateBot()

client.on('disconnect', function(erMsg, code) {
    console.log('----- Translate disconnected from Discord with code', code, 'for reason:', erMsg, '-----');
    TranslateBot.connect();
});

client.on('ready', () => {
    console.log('----- TranslateBot Connected. Logged In As: '+client.user.tag+" -----");
});

client.on('message', message => {
    if(message.author == client.user){
        return;
    }
    else{
        var commands = message.content.split(" ");
        var url = ""
        if(commands[0] == delimiter+"translate"){
            var translateString = ""
            commands.forEach((command, index) => {
                if(index > 1){
                    if(index == 2){
                        translateString = command
                    }
                    else{
                        translateString = translateString + " " + command
                    }
                }
            })
            if((commands[1].toUpperCase() == "ARABIC") || (commands[1].toUpperCase() == "AR")){
                url = translateURL + "ar&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "BASQUE") || (commands[1].toUpperCase() == "EU")){
                url = translateURL + "eu&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "BENGALI") || (commands[1].toUpperCase() == "BN")){
                url = translateURL + "bn&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "BULGARIAN") || (commands[1].toUpperCase() == "BG")){
                url = translateURL + "bg&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "CATALAN") || (commands[1].toUpperCase() == "CA")){
                url = translateURL + "ca&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "CHEROKEE") || (commands[1].toUpperCase() == "CHR")){
                url = translateURL + "chr&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "CROATIAN") || (commands[1].toUpperCase() == "HR")){
                url = translateURL + "hr&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "DANISH") || (commands[1].toUpperCase() == "DA")){
                url = translateURL + "da&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "ESTONIAN") || (commands[1].toUpperCase() == "ET")){
                url = translateURL + "et&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "FILIPINO") || (commands[1].toUpperCase() == "FIL")){
                url = translateURL + "fil&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "FINNISH") || (commands[1].toUpperCase() == "FI")){
                url = translateURL + "fi&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "GREEK") || (commands[1].toUpperCase() == "EL")){
                url = translateURL + "el&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "GUJARATI") || (commands[1].toUpperCase() == "GU")){
                url = translateURL + "gu&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "HEBREW") || (commands[1].toUpperCase() == "IW")){
                url = translateURL + "iw&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "HINDI") || (commands[1].toUpperCase() == "HI")){
                url = translateURL + "hi&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "HUNGARIAN") || (commands[1].toUpperCase() == "HU")){
                url = translateURL + "hu&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "ICELANDIC") || (commands[1].toUpperCase() == "IS")){
                url = translateURL + "is&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "INDONESIAN") || (commands[1].toUpperCase() == "ID")){
                url = translateURL + "id&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "KANNADA") || (commands[1].toUpperCase() == "KN")){
                url = translateURL + "kn&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "LATVIAN") || (commands[1].toUpperCase() == "LV")){
                url = translateURL + "lv&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "LITHUANIAN") || (commands[1].toUpperCase() == "LT")){
                url = translateURL + "lt&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "MALAY") || (commands[1].toUpperCase() == "MS")){
                url = translateURL + "ms&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "MALAYALAM") || (commands[1].toUpperCase() == "ML")){
                url = translateURL + "ml&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "MARATHI") || (commands[1].toUpperCase() == "MR")){
                url = translateURL + "mr&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "ROMANIAN") || (commands[1].toUpperCase() == "RO")){
                url = translateURL + "ro&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "SERBIAN") || (commands[1].toUpperCase() == "SR")){
                url = translateURL + "sr&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "SLOVAK") || (commands[1].toUpperCase() == "SK")){
                url = translateURL + "sk&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "SLOVENIAN") || (commands[1].toUpperCase() == "SL")){
                url = translateURL + "sl&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "SWAHILI") || (commands[1].toUpperCase() == "SW")){
                url = translateURL + "sw&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "TAMIL") || (commands[1].toUpperCase() == "TA")){
                url = translateURL + "ta&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "TELUGU") || (commands[1].toUpperCase() == "TE")){
                url = translateURL + "te&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "THAI") || (commands[1].toUpperCase() == "TH")){
                url = translateURL + "th&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "TURKISH") || (commands[1].toUpperCase() == "TR")){
                url = translateURL + "tr&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "URDU") || (commands[1].toUpperCase() == "UR")){
                url = translateURL + "ur&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "UKRAINIAN") || (commands[1].toUpperCase() == "UK")){
                url = translateURL + "uk&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "VIETNAMESE") || (commands[1].toUpperCase() == "VI")){
                url = translateURL + "vi&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "WELSH") || (commands[1].toUpperCase() == "CY")){
                url = translateURL + "cy&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "AMHARIC") || (commands[1].toUpperCase() == "AM")){
                url = translateURL + "am&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "ENGLISH") || (commands[1].toUpperCase() == "EN")){
                url = translateURL + "en&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "FRENCH") || (commands[1].toUpperCase() == "FR")){
                url = translateURL + "fr&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "SPANISH") || (commands[1].toUpperCase() == "ES")){
                url = translateURL + "es&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "BULGARIAN") || (commands[1].toUpperCase() == "BG")){
                url = translateURL + "bg&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "CHINESE") || (commands[1].toUpperCase() == "ZH")){
                url = translateURL + "zh&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "CZECH") || (commands[1].toUpperCase() == "CS")){
                url = translateURL + "cs&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "DUTCH") || (commands[1].toUpperCase() == "NL")){
                url = translateURL + "nl&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "GERMAN") || (commands[1].toUpperCase() == "DE")){
                url = translateURL + "de&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "ITALIAN") || (commands[1].toUpperCase() == "IT")){
                url = translateURL + "it&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "JAPANESE") || (commands[1].toUpperCase() == "JP")){
                url = translateURL + "jp&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "KOREAN") || (commands[1].toUpperCase() == "KO")){
                url = translateURL + "ko&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "LATIN") || (commands[1].toUpperCase() == "LA")){
                url = translateURL + "la&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "NORWEGIAN") || (commands[1].toUpperCase() == "NO")){
                url = translateURL + "no&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "POLISH") || (commands[1].toUpperCase() == "PL")){
                url = translateURL + "pl&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "PORTUGUESE") || (commands[1].toUpperCase() == "PT")){
                url = translateURL + "pt&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "RUSSIAN") || (commands[1].toUpperCase() == "RU")){
                url = translateURL + "ru&dt=t&q=" + encodeURI(translateString);
            }
            if((commands[1].toUpperCase() == "SWEDISH") || (commands[1].toUpperCase() == "SV")){
                url = translateURL + "sv&dt=t&q=" + encodeURI(translateString);
            }
            if(url != ""){
                var options = {
                    url: url,
                    headers: {
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0'
                    }
                }
                request.get(options, (error, response, body) => {
                    if(error && response.statusCode != 200){
                        console.error(response.statusCode);
                        console.error(body);
                    }
                    else{
                        var data = JSON.parse(body);
                        message.reply(data[0][0][0])
                    }
                });
            }
        }
    }
});

client.login(authToken)