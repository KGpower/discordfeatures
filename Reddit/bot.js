const Discord = require('discord.js');
const request = require('request')
const url_template = ["https://www.reddit.com/r/","/top/.json?"]
var arguments = process.argv.slice(2)
var authToken = arguments[0]
var delimiter = arguments[1]

class RedditBot extends Discord.Client {
    constructor() {
        super({token: authToken, autorun: true});
    }
}

var client = new RedditBot()

client.on('disconnect', function(erMsg, code) {
    console.log('----- RedditBot disconnected from Discord with code', code, 'for reason:', erMsg, '-----');
    RedditBot.connect();
});

client.on('ready', () => {
    console.log('----- RedditBot Connected. Logged In As: '+client.user.tag+" -----");
});

client.on('message', message => {
    if(message.author == client.user){
        return;
    }
    else{
        var commands = message.content.split(" ");
        if(commands[0] == delimiter+"reddit"){
            if(commands.length == 2){
                var url = url_template[0]+commands[1]+url_template[1]
                request.get(url, (error, response, body) => {
                    if(error && response.code != 200){
                        console.error(response.statusCode)
                        console.error(body)
                    }
                    else{
                        var data = JSON.parse(body)
                        var counter = 0
                        for(var post of data["data"]["children"]){
                            var messageEmbed = new Discord.MessageEmbed();
                            if (counter < 5){
                                var sent = false
                                try{
                                    var url = post["data"]["url"]
                                    if(url.includes("gfycat")){
                                        url = url + ".gifv"
                                    }
                                    else if(url.includes("imgur")){
                                        url = url + ".jpg"
                                    }
                                    if(url.includes(".gifv")){
                                        message.reply(url)
                                        sent = true

                                    }
                                    else if(url.includes("redgifs")){
                                        url = url.replace("watch", "ifr")
                                        message.reply(url)
                                        sent = true
                                    }
                                    else{
                                        messageEmbed.setAuthor(post["data"]["author"], post["data"]["thumbnail"], "https://www.reddit.com/u/" + post["data"]["author"])
                                        messageEmbed.setTitle(post["data"]["title"])
                                        messageEmbed.setColor("#FFFFFF")
                                        messageEmbed.setImage(url)
                                        messageEmbed.setURL("https://www.reddit.com"+post["data"]["permalink"])
                                        messageEmbed.addFields(
                                            {name: "Score: ", value: post["data"]["score"], inline: true}
                                        )
                                    }

                                }
                                catch{
                                    messageEmbed.setDescription("Image Not Found")
                                    messageEmbed.setImage("https://help.imgur.com/hc/article_attachments/115003789423/sadgiraffe.png")
                                }
                                if(sent == false){
                                    message.reply(messageEmbed)  
                                }                                  
                                counter += 1
                            }
                            else{
                                break
                            }
                        }
                    }
                })
            }
            if(commands.length == 3){
                if(commands[1].toUpperCase() == "USER"){
                    var url = "https://www.reddit.com/user/"+commands[2]+"/top/.json?count=20"
                    request.get(url, (error, response, body) => {
                        if(error && response.code != 200){
                            console.error(response.statusCode)
                            console.error(body)
                        }
                        else{
                            var data = JSON.parse(body)
                            var counter = 0
                            for(var post of data["data"]["children"]){
                                var messageEmbed = new Discord.MessageEmbed();
                                if (counter < 5){
                                    var sent = false
                                    try{
                                        var url = post["data"]["url"]
                                        if(url.includes("gfycat")){
                                            url = url + ".gifv"
                                        }
                                        else if(url.includes("imgur")){
                                            url = url + ".jpg"
                                        }
                                        if(url.includes(".gifv")){
                                            message.reply(url)
                                            sent = true
    
                                        }
                                        else if(url.includes("redgifs")){
                                            url = url.replace("watch", "ifr")
                                            message.reply(url)
                                            sent = true
                                        }
                                        else{
                                            messageEmbed.setAuthor(post["data"]["author"], post["data"]["thumbnail"], "https://www.reddit.com/u/" + post["data"]["author"])
                                            messageEmbed.setTitle(post["data"]["title"])
                                            messageEmbed.setColor("#FFFFFF")
                                            messageEmbed.setImage(url)
                                            messageEmbed.setURL("https://www.reddit.com"+post["data"]["permalink"])
                                            messageEmbed.addFields(
                                                {name: "Score: ", value: post["data"]["score"], inline: true}
                                            )
                                        }
    
                                    }
                                    catch{
                                        messageEmbed.setDescription("Image Not Found")
                                        messageEmbed.setImage("https://help.imgur.com/hc/article_attachments/115003789423/sadgiraffe.png")
                                    }
                                    if(sent == false){
                                        message.reply(messageEmbed)  
                                    }                                  
                                    counter += 1
                                }
                                else{
                                    break
                                }
                            }
                        }
                    })
                }
                else{
                    var url = url_template[0]+commands[1]+url_template[1]
                if(commands[2].toUpperCase() == "ALL"){
                    url = url + "t=all"
                }
                if(commands[2].toUpperCase() == "YEAR"){
                    url = url + "t=year"
                }
                if(commands[2].toUpperCase() == "MONTH"){
                    url = url + "t=month"
                }
                if(commands[2].toUpperCase() == "WEEK"){
                    url = url + "t=week"
                }
                if(commands[2].toUpperCase() == "DAY"){
                    url = url + "t=day"
                }
                request.get(url, (error, response, body) => {
                    if(error && response.code != 200){
                        console.error(response.statusCode)
                        console.error(body)
                    }
                    else{
                        var data = JSON.parse(body)
                        var counter = 0
                        for(var post of data["data"]["children"]){
                            var messageEmbed = new Discord.MessageEmbed();
                            if (counter < 5){
                                var sent = false
                                try{
                                    var url = post["data"]["url"]
                                    if(url.includes("gfycat")){
                                        url = url + ".gifv"
                                    }
                                    else if(url.includes("imgur")){
                                        url = url + ".jpg"
                                    }
                                    if(url.includes(".gifv")){
                                        message.reply(url)
                                        sent = true

                                    }
                                    else if(url.includes("redgifs")){
                                        url = url.replace("watch", "ifr")
                                        message.reply(url)
                                        sent = true
                                    }
                                    else{
                                        messageEmbed.setAuthor(post["data"]["author"], post["data"]["thumbnail"], "https://www.reddit.com/u/" + post["data"]["author"])
                                        messageEmbed.setTitle(post["data"]["title"])
                                        messageEmbed.setColor("#FFFFFF")
                                        messageEmbed.setImage(url)
                                        messageEmbed.setURL("https://www.reddit.com"+post["data"]["permalink"])
                                        messageEmbed.addFields(
                                            {name: "Score: ", value: post["data"]["score"], inline: true}
                                        )
                                    }

                                }
                                catch{
                                    messageEmbed.setDescription("Image Not Found")
                                    messageEmbed.setImage("https://help.imgur.com/hc/article_attachments/115003789423/sadgiraffe.png")
                                }
                                if(sent == false){
                                    message.reply(messageEmbed)  
                                }                                  
                                counter += 1
                            }
                            else{
                                break
                            }
                        }
                    }
                })
                }
            }
        }
    }
});

client.login(authToken)