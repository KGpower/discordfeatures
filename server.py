from flask import Flask, render_template
from flask_restful import Resource, Api, abort

import subprocess as sp
import multiprocessing as mp
import time

app = Flask(__name__)

bots = {
        "Translator": {"token": "NzEyNjI3MDM2MzYzMTYxNjYw.XsUTzA.aY0N3PQoM0hdC93TCc17SG9QCMk", "location": "Translator/bot.js" , "delimiter": "?", "process": None},
        "Reddit": {"token": "NzEzMzA2Mjc1OTgyNzM3NDE5.XseMTQ.KIhjs2XHYJ0n8kxnr9iR-eT1o1k", "location": "Reddit/bot.js" , "delimiter": "!", "process": None},
        "Timer": {"token": "NzExMjIxMzA5NjM1Mjk3Mjgw.Xr_4qg.Z_-wK6yjrgmsrh9ThMNOZy5Lgsg", "location": "Timer/bot.js" , "delimiter": "!", "process": None},
        "GameStats": {"token": "NzEyNjg1ODE3MjM5MTc1MjIw.XsVKiQ.ezx9PcxLF4wyoUfX3Pooyq0ByOg", "location": "GameStats/bot.js" , "delimiter": "!", "process": None},
}

def launch_bot(token, location, delimiter):
    sp.run(['node', location, token, delimiter])

def create_process(bot):
    if bot["process"] != None:
        if bot["process"].is_alive():
            print("Process {pid} is still alive. Not restarting.".format(pid=bot["process"].pid))
            return
    bot["process"] = mp.Process(target=launch_bot, args=(bot["token"], bot["location"], bot["delimiter"]))
    bot["process"].start()

if __name__ == "__main__":
    for bot in bots:
        create_process(bots[bot])
    while True:
        time.sleep(1)
        for bot in bots:
            if not bots[bot]["process"].is_alive():
                print("!!!! Process {pid} is Dead. Restarting...  !!!!".format(pid=bots[bot]["process"].pid))
                create_process(bots[bot])
        time.sleep(5)